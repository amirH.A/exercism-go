// Package greeting is an exercise package that exports a HelloWorld func
package greeting

// HelloWorld returns the "Hello, World!" string
func HelloWorld() string {
	return "Hello, World!"
}
