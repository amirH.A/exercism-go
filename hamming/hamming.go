// Package hamming contains utilities to calculate the hamming distance between two strings
package hamming

import (
	"errors"
	"unicode/utf8"
)

// Distance returns the hamming distance between 2 strings of equal length.
// An error is returned when the invalid inputs are provided
func Distance(a, b string) (int, error) {
	if len(a) != len(b) {
		return 0, errors.New("inputs are not of equal length")
	}

	count := 0

	for i, w := 0, 0; i < len(a); i += w {
		runeValueA, width := utf8.DecodeRuneInString(a[i:])
		runeValueB, _ := utf8.DecodeRuneInString(b[i:])
		if runeValueA != runeValueB {
			count++
		}
		w = width
	}
	return count, nil
}
