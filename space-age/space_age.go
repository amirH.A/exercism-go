package space

// Planet type is used for representing a planet
type Planet string

// Age returns the how many years on a given planet, the given seconds are
func Age(seconds float64, planet Planet) float64 {
	return seconds / secondsInYear(planet)
}

func secondsInYear(planet Planet) float64 {
	const SecondsInEarthYear = 31557600
	planetSecondsInYear := map[Planet]float64{
		"Mercury": 0.2408467 * SecondsInEarthYear,
		"Venus":   0.61519726 * SecondsInEarthYear,
		"Earth":   SecondsInEarthYear,
		"Mars":    1.8808158 * SecondsInEarthYear,
		"Jupiter": 11.862615 * SecondsInEarthYear,
		"Saturn":  29.447498 * SecondsInEarthYear,
		"Uranus":  84.016846 * SecondsInEarthYear,
		"Neptune": 164.79132 * SecondsInEarthYear,
	}
	return planetSecondsInYear[planet]
}
