// Package leap contains utility for finding out if a year is a leap year
package leap

// IsLeapYear returns whether a given year is a leap year
func IsLeapYear(year int) bool {
	// a year is a leap yar if it's divisible by 4 unless it's divisible by 100
	// however if it's divisible by 400, it's a leap year as well
	return year%4 == 0 && ((year%100 != 0) || (year%400 == 0))
}
