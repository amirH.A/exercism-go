// Package triangle provides utilities for categorising triangles types
package triangle

import "math"

// Kind specifies which kind of a triangle a given triangle is
type Kind int

const (
	// NaT = not a triangle
	NaT Kind = iota
	// Equ = equilateral
	Equ
	// Iso = isosceles
	Iso
	// Sca = scalene
	Sca
)

// valueIsInvalid returns true if value
func valueIsInvalid(num float64) bool {
	return num == 0 || math.IsNaN(num) || math.IsInf(num, 0)
}

// KindFromSides returns which kind a triangle is, given length of its sidess
func KindFromSides(a, b, c float64) Kind {
	if a+b < c || a+c < b || b+c < a || valueIsInvalid(a) || valueIsInvalid(b) || valueIsInvalid(c) {
		return NaT
	}

	if a == b && b == c {
		return Equ
	}

	if a == b || a == c || b == c {
		return Iso
	}

	return Sca
}
