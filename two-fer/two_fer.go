// Package twofer contains the code for the twofer exercise
package twofer

import "fmt"

// ShareWith returns formattetd string for the twofer problem
func ShareWith(name string) string {
	if name == "" {
		name = "you"
	}
	return fmt.Sprintf("One for %s, one for me.", name)
}
